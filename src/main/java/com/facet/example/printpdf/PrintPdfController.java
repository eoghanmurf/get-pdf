package com.facet.example.printpdf;

import org.springframework.core.io.FileSystemResource;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.bind.annotation.RestController;

@RestController
public class PrintPdfController{ 

    @CrossOrigin
	@RequestMapping(value="/postpdf", method=RequestMethod.POST, produces = "application/pdf")	
	public @ResponseBody FileSystemResource postpdf() throws Exception{
		
		String fileName = "C://Users/Eoghan.Murphy/Downloads/test.pdf";
		
		FileSystemResource result = new FileSystemResource(fileName);

		return result;
			
	}

	@RequestMapping(value="/getpdf", method=RequestMethod.GET, produces = "application/pdf")	
	public @ResponseBody FileSystemResource getpdf() throws Exception{
		
		String fileName = "C://Users/Eoghan.Murphy/Downloads/test.pdf";
		
		FileSystemResource result = new FileSystemResource(fileName);

		return result;
			
	}
}
