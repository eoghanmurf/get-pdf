package com.facet.example.printpdf;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class PrintpdfApplication {

	public static void main(String[] args) {
		SpringApplication.run(PrintpdfApplication.class, args);
	}
}
